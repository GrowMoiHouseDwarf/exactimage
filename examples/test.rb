#!/usr/bin/ruby 

# ExactImage Ruby Example
# Copyright (C) 2016 - 2017 Rene Rebe, ExactCODE GmbH

# add to module search path
$:.unshift File.dirname(__FILE__) + "/../objdir/api/ruby"

# load the module
require 'ExactImage'

image = ExactImage::newImage()

ret = ExactImage::decodeImageFile(image, "../testsuite/230-testimg.jpg")
if not ret
  puts 'failed to open image'
else
  puts ExactImage::imageWidth(image)
  puts ExactImage::imageHeight(image)

  ExactImage::encodeImageFile(image, "test.jpg")
end

ExactImage::deleteImage(image)
